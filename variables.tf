# Required variables
variable "server_port" {
  description = "Port for web server"
  default = 8080
}
